#くじゲームの作成
from random import randint

user_num = []       
lucky_num = []

print("１～１０の数字を入力してください")

#抽選エントリー用の数字を選ぶ
while 0 <= len(user_num) < 5:    #5回抽選を実施
    input_num = input('> ')
    
    try:
        a = int(input_num)
    except:
        print("無効な数字です")     #数字以外の時に表示
        continue
        
    if 0 > a or a > 10:            #1~10以外の時の処理
        print("１～１０の範囲です")
        continue
    elif a in user_num:　　　　　　　#同じ数字の入力時の処理
        print(user_num,"以外の数字を入力してください")
        continue
    user_num.append(a)
print("あなたが選んだ数字は",user_num,"です。")

#当選番号を選ぶ
print("抽選をはじめます")
while 0 <= len(lucky_num) <5 :
    b =randint(1,10)
    if b not in lucky_num:       #同じ番号がふくませていないか？
        lucky_num.append(b)
    else:
        continue
print(lucky_num)

#抽選エントリー番号と当選番号の比較
userset = set(user_num)
luckyset = set(lucky_num)
winset = userset.intersection(luckyset)  #集合の共通要素を抜き出す　　userset luckysetの共通番号は？
print("当選した数字は",winset)
print("当選数は",len(winset),"個です\n")