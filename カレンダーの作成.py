#月単位 or 年間のカレンダーを表示し、祝日を表示する

import calendar
import datetime
import jpholiday

year = int(input("西暦を入力してください"))
num = int(input("カレンダーの表示方法を選択、１：月間、２：年間　を表示"))
if num ==1:
    month = int(input("月を入力してください"))
    holiday_month = jpholiday.month_holidays(year, month)
    print(calendar.month(year, month, w=3, l=2))
    print(holiday_month)
    
elif num ==2:
    print(calendar.calendar(year))
    for i in range(1,13):
        holiday_month = jpholiday.month_holidays(year, i)
        print(holiday_month)
else:
    print("選択した数字が正しくありません")
    